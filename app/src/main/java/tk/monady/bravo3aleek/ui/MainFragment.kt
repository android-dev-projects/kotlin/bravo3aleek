package tk.monady.bravo3aleek.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import tk.monady.bravo3aleek.R
import tk.monady.bravo3aleek.Utils.MusicPlayerUtil
import tk.monady.bravo3aleek.databinding.FragmentMainBinding
import tk.monady.bravo3aleek.viewmodel.AchievementViewModel

class MainFragment : Fragment() {

    lateinit var binding: FragmentMainBinding
    val viewModel: AchievementViewModel by viewModels()
    val musicPlayer: MusicPlayerUtil = MusicPlayerUtil()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(layoutInflater)

        binding.viewModel = viewModel

        viewModel.beginCeleb.observe(viewLifecycleOwner){
            it?.let{
                if(it){
                    if(binding.editTextTextMultiLine.text.isNotEmpty()) {
                        musicPlayer.play(requireContext(), R.raw.bravo3aleek)
                    }
                    else
                        musicPlayer.play(requireContext(), R.raw.yasalam)

                    if(!musicPlayer.musicPlayer!!.isPlaying){
                        musicPlayer.stop()
                    }
                }
            }
        }
        // Inflate the layout for this fragment
        return binding.root
    }
}