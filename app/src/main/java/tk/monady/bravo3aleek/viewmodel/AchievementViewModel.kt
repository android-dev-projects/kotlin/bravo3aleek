package tk.monady.bravo3aleek.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AchievementViewModel: ViewModel() {

    private val _beginCeleb= MutableLiveData<Boolean>()
    val beginCeleb: LiveData<Boolean>
        get() = _beginCeleb

    fun initiateCeleb(){
        _beginCeleb.postValue(true)
    }

    fun terminateCeleb(){
        _beginCeleb.postValue(false)
    }

}