package tk.monady.bravo3aleek.Utils

import android.content.Context
import android.media.MediaPlayer

class MusicPlayerUtil() {
    var musicPlayer: MediaPlayer? = null

    private fun getInstance(context: Context, audioId: Int): MediaPlayer?{
        if(musicPlayer == null)
            musicPlayer = MediaPlayer.create(context, audioId)
        return musicPlayer
    }

    fun play(context: Context, audioId: Int){
        //musicPlayer = getInstance(context, audioId)
        musicPlayer = MediaPlayer.create(context, audioId)
        musicPlayer?.start()
        if(!musicPlayer?.isPlaying!!)
            stop()
    }

    fun stop(){
        if(musicPlayer != null) {
            musicPlayer?.stop()
            musicPlayer?.release()
        }
    }
}